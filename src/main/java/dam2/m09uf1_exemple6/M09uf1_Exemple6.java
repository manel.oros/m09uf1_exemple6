/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package dam2.m09uf1_exemple6;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

/**
 * Exemple de servei a Debian-based (Ubuntu 20.04 LTS).
 * 
 * Aplicació que elimina fitxers temporals cada cert interval de temps. 
 * 
 * Per a convertir aquesta aplicació en un servei Debian-based (Ubuntu 20.04 LTS), seguir els següets passos:
 * 
 * 1 - Generar un FatJar i verificar que executa correctament des d'un terminal
 * 2 - Crear un fitxer anomenat /etc/systemd/system/m09uf1_exemple6.service amb el següent contingut (adaptar els camps User, Group, ExecStart a l'entorn d'execució local):
 * 
 * 
        [Unit]
        Description=Exemple de servei Java
        After=syslog.target network.target

        [Service]
        Type=simple
        SuccessExitStatus=143
        User=manel
        Group=manel
        ExecStart=/usr/lib/jvm/java-17-openjdk-amd64/bin/java -jar /home/manel/NetBeansProjects/m09uf1_Exemple6/target/m09uf1_Exemple6.jar
        ExecStop=/bin/kill -15 $MAINPID

        [Install]
        WantedBy=multi-user.target

 *  
 * 3 - Donar d'alta el servei amb: sudo systemctl daemon-reload
 * 4 - Activar el servei amb: sudo systemctl start m09uf1_exemple6.service
 * 5 - Verificar que ha iniciat correctament amb: sudo systemctl status m09uf1_exemple6.service
 * 6 - Verificar que esborra els fitxers 
 * 
 * @author manel
 */
public class M09uf1_Exemple6 {
    
    // determina si s'està executant el mètode run
    static boolean running = false;

    public static void main(String[] args) {
        
        // definició del listener com a un fil per realitzar una tasca
        var shutdownListener = new Thread(){
            
            // aquesta és la tasca
            @Override
            public void run(){
                // en cas de que s'estigui em meitat d'un esborrat, espera un temps prudencial (10 segons) a finalitzar l'aplicació. 
                if (running){
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {}
                }
            }
        };
        
        // afegim el listener a l'esdeveniment "ShutdownHook", que es dispara quan el sistema operatiu demana tancar l'aplicació
        Runtime.getRuntime().addShutdownHook(shutdownListener);

        //mecanisme que ens permet executar cada cert temps una tasca com a thread. En aquest cas, la tasca d'esborrat de fixters.
        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(()
                //codi a executar, temps fins a la primera execució, temps entre execucions, unitat de temps
                -> deleteFiles() , 10, 10, TimeUnit.SECONDS);
        
    }
    
    /***
     * Elimina els fitxers temporals cada 10 segons, sense recursivitat (no subdirs).
     * Es consideren fixters temporals aquells que estan continguts a /tmp del directori personal i amb extensió *.tmp.
     */
    private static void deleteFiles()
    {
        //activem semàfor
        running = true;
        
        //directori de treball
        String sourceDir = System.getProperty("user.home") + "/tmp";
        
        try {
            
            //path al directori
            Path dir = Paths.get(sourceDir); 
            
            //recollim tot el seu contingut
            Stream<Path> s = Files.list(dir);
            
            for (Path p : s.toList()){
                
                if (! p.toFile().isDirectory() && p.toString().endsWith("tmp"))
                {
                    Files.delete(p);
                }
            }
           
        } catch (IOException ex) {
            
            //si es produeix una excepció, generem un fixter d'error amb el missatge
            try {
                //preparem per escriure
                BufferedWriter writer = new BufferedWriter(new FileWriter(sourceDir + "/error.txt", true));
                //escrivim
                writer.append((CharSequence) ex);
                //tanquem
                writer.close();
                        
            } catch (IOException ex1) {}
            
        } finally{
            
             //tanquem semàfor
             running = false;
        }
    }
}
